<?

namespace Simple\Orm;

use Simple\Orm\TestTable,
	Bitrix\Main;

class Test implements DefaultOrm
{
	/**
	 * @param array $arrOrder
	 * @param array $arrFilter
	 * @param array $arrSelect
	 * @param array $arrNav
	 * @return object CDBResult
	 */
	public static function getList($arrOrder = array(), $arrFilter = array(), $arrSelect = array(), $arrNav = array('offset' => 0, 'limit' => 0))
	{
		if (!is_array($arrOrder) || !is_array($arrFilter) || !is_array($arrSelect))
		{
			throw new Main\ArgumentTypeException('$arrOrder || $arrFilter || $arrSelect');
		}

		if (!is_array($arrNav))
		{
			$arrNav = array('offset' => 0, 'limit' => 0);
		}
		else
		{
			$arrNav['offset'] = intVal($arrNav['offset']);

			$arrNav['limit'] = intVal($arrNav['limit']);
		}

		if (empty($arrSelect))
		{
			$arrSelect = array('*');
		}

		$objResult = TestTable::getList(
			array(

				'select' => $arrSelect,
				'filter' => $arrFilter,
				'order' => $arrOrder,
				'offset' => $arrNav['offset'],
				'limit' => $arrNav['limit'],
			));


		return $objResult;
	}

	/**
	 * @param int $intId
	 * @return array
	 */

	public static function getById($intId = 0)
	{
		$intId = intVal($intId);

		if (empty($intId))
		{
			throw new Main\ArgumentNullException('ID');
		}

		$arrResult = array();

		$objResult = self::getList(array('ID' => 'DESC'), array('ID' => $intId), array('*', 'FULL_NAME', 'LOGIN' => 'USER.LOGIN'), array('limit' => 1));

		$arrResult = $objResult->fetch();

		return $arrResult;
	}

	/**
	 * @param array $arrFields
	 * @return bool
	 */

	public static function add($arrFields = array())
	{
		if (!is_array($arrFields))
		{
			throw new Main\ArgumentTypeException('$arrFields');
		}

		if (empty($arrFields['TITLE']) || !is_string($arrFields['TITLE']))
		{
			throw new Main\ArgumentTypeException('TITLE');
		}

		if (empty($arrFields['USER_ID']) || !is_string($arrFields['USER_ID']))
		{
			throw new Main\ArgumentTypeException('USER_ID');
		}

		$objResult = TestTable::add($arrFields);

		if ($objResult->isSuccess())
		{
			return $objResult->getId();
		}
		else
		{
			return false;
		}
	}

	/**
	 * @param int $intId
	 * @param array $arrFields
	 * @return bool
	 */

	public static function update($intId = 0, $arrFields = array())
	{
		if (!empty($intId))
		{
			$intId = intVal($intId);
		}

		if (empty($intId))
		{
			throw new Main\ArgumentNullException('ID');
		}

		if (!is_array($arrFields))
		{
			throw new Main\ArgumentTypeException('$arrFields');
		}

		$objResult = TestTable::update($intId, $arrFields);

		if ($objResult->isSuccess())
		{
			return $objResult->getId();
		}
		else
		{
			return false;
		}
	}

	/**
	 * @param int $intId
	 * @return bool
	 */

	public static function delete($intId = 0)
	{
		if (!empty($intId))
		{
			$intId = intVal($intId);
		}

		if (empty($intId))
		{
			throw new Main\ArgumentNullException('ID');
		}

		$objResult = TestTable::delete($intId);

		if ($objResult->isSuccess())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}