<?

use Bitrix\Main\ModuleManager,
	Bitrix\Main\Application,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\EventManager,
	Bitrix\Main\UrlRewriter,
	Bitrix\Main\IO\Directory,
	Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

Class simple_orm extends \CModule
{
	public $MODULE_ID;
	public $COMPONENT_PATH_ID = 'orm';
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	public $errors;

	public function __construct()
	{
		$this->MODULE_ID = str_replace('_', '.', toLower(__CLASS__));

		$this->MODULE_VERSION = '0.0.1';

		$this->MODULE_VERSION_DATE = '27.09.2018';

		$this->MODULE_NAME = Loc::getMessage('SO_MODULE_NAME');

		$this->MODULE_DESCRIPTION = Loc::getMessage('SO_MODULE_DESCR');

		$this->PARTNER_NAME = '';

		$this->PARTNER_URI = '';
	}

	public function DoInstall()
	{
		$this->InstallDB();

		$this->InstallEvents();

		$this->InstallFiles();

		$this->addUrlRewrite();

		ModuleManager::RegisterModule($this->MODULE_ID);

		Option::set($this->MODULE_ID, 'uf_code', 'ENTITY_TEST_ENTITY');

		$GLOBALS['APPLICATION']->IncludeAdminFile(
			Loc::getMessage('SO_INSTALL_TITLE') . ' "' . Loc::getMessage('SO_MODULE_NAME') . '"',
			__DIR__ . '/step.php'
		);

		return true;
	}

	public function DoUninstall()
	{
		$this->UnInstallDB();

		$this->UnInstallEvents();

		$this->UnInstallFiles();

		ModuleManager::unRegisterModule($this->MODULE_ID);

		$GLOBALS['APPLICATION']->IncludeAdminFile(
			Loc::getMessage('SO_UNINSTALL_TITLE') . ' "' . Loc::getMessage('SO_MODULE_NAME') . '"',
			__DIR__ . '/unstep.php'
		);

		return true;
	}

	public function InstallDB()
	{
		global $DB;

		$this->errors = false;

		$this->errors = $DB->RunSQLBatch(__DIR__ . '/db/install.sql');

		if (empty($this->errors))
		{
			return true;
		}
		else
			return $this->errors;
	}

	public function UnInstallDB()
	{
		global $DB;

		$this->errors = false;

		$this->errors = $DB->RunSQLBatch(__DIR__ . '/db/uninstall.sql');

		if (empty($this->errors))
		{
			return true;
		}
		else
			return $this->errors;
	}

	public function InstallEvents()
	{
		return true;
	}

	public function UnInstallEvents()
	{
		return true;
	}

	public function InstallFiles()
	{

		$boolDir = Directory::isDirectoryExists(
			Application::getDocumentRoot() . '/local/components/' . $this->COMPONENT_PATH_ID . '/'
		);

		if (!empty($boolDir))
		{
			$this->copyOnlyComponents();
		}
		else
		{
			CopyDirFiles(
				__DIR__ . '/components/',
				Application::getDocumentRoot() . '/local/components/' . $this->COMPONENT_PATH_ID . '/',
				true,
				true
			);
		}

		$this->installPublic();


		return true;
	}

	public function installPublic()
	{
		CopyDirFiles(
			__DIR__ . '/public/',
			Application::getDocumentRoot() . '/',
			true,
			true
		);
	}

	public function UnInstallFiles()
	{
		$this->deleteOnlyComponents();

		$this->deleteComponentPath();

		return true;
	}

	public function addUrlRewrite()
	{
		$objSites = CSite::GetList($by = 'sort', $order = 'desc', Array('ACTIVE' => 'Y'));

		while ($arrSite = $objSites->Fetch())
		{
			$arrRules = UrlRewriter::getList($arrSite['LID'], array('CONDITION' => '#^/test/#'));

			if (sizeof($arrRules) > 0)
			{
				UrlRewriter::update(
					$arrSite['LID'],
					array('CONDITION' => '#^/test/#'),
					array(
						'CONDITION' => '#^/test/#',
						'ID' => 'orm:entity.actions',
						'PATH' => '/test/index.php',
						'RULE' => 100
					)
				);
			}
			else
			{
				UrlRewriter::add(
					$arrSite['LID'],
					array(
						'CONDITION' => '#^/test/#',
						'ID' => 'orm:entity.actions',
						'PATH' => '/test/index.php',
						'RULE' => 100
					)
				);
			}
		}
	}

	public function copyOnlyComponents()
	{
		$arrDirectory = array_diff(scandir(__DIR__ . '/components/'), array('..', '.'));

		if (!empty($arrDirectory))
		{
			foreach ($arrDirectory as $strDirectory)
			{

				$objNewDirectory = Directory::createDirectory(Application::getDocumentRoot() . '/local/components/' . $this->COMPONENT_PATH_ID . '/' . $strDirectory);

				CopyDirFiles(
					__DIR__ . '/components/' . $strDirectory . '/',
					$objNewDirectory->getPath(),
					true,
					true
				);
			}
		}
	}

	public function deleteOnlyComponents()
	{
		$arrDirectory = array_diff(scandir(__DIR__ . '/components/'), array('..', '.'));

		if (!empty($arrDirectory))
		{
			foreach ($arrDirectory as $strDirectory)
			{
				$boolDir = Directory::isDirectoryExists(
					Application::getDocumentRoot() . '/local/components/' . $this->COMPONENT_PATH_ID . '/' . $strDirectory
				);

				if (!empty($boolDir))
				{
					Directory::deleteDirectory(
						Application::getDocumentRoot() . '/local/components/' . $this->COMPONENT_PATH_ID . '/' . $strDirectory
					);
				}
			}
		}
	}

	public function deleteComponentPath()
	{
		$boolDir = Directory::isDirectoryExists(
			Application::getDocumentRoot() . '/local/components/' . $this->COMPONENT_PATH_ID . '/'
		);

		if (!empty($boolDir))
		{
			$objDirectory = new Bitrix\Main\IO\Directory(Application::getDocumentRoot() . '/local/components/' . $this->COMPONENT_PATH_ID . '/');

			$arrChildren = $objDirectory->getChildren();

			if (empty($arrChildren))
			{
				Directory::deleteDirectory(
					Application::getDocumentRoot() . '/local/components/' . $this->COMPONENT_PATH_ID . '/'
				);
			}
		}
	}
}