<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$arComponentParameters = array(
	'PARAMETERS' => array(
		'VARIABLE_ALIASES' => Array(
			'ENTITY_ID' => Array('NAME' => Loc::getMessage('EA_ENTITY_ID')),
		),
		'SEF_MODE' => Array(
			'list' => array(
				'NAME' => Loc::getMessage('EA_SEF_MODE_LIST'),
				'DEFAULT' => '',
				'VARIABLES' => array(),
			),
			'add' => array(
				'NAME' => Loc::getMessage('EA_SEF_MODE_ADD'),
				'DEFAULT' => '',
				'VARIABLES' => array(),
			),
			'edit' => array(
				'NAME' => Loc::getMessage('EA_SEF_MODE_EDIT'),
				'DEFAULT' => '',
				'VARIABLES' => array('ENTITY_ID'),
			),
			'detail' => array(
				'NAME' => Loc::getMessage('EA_SEF_MODE_DETAIL'),
				'DEFAULT' => '#ENTITY_ID#/',
				'VARIABLES' => array('ENTITY_ID'),
			)
		),
		'PAGE_COUNT' => Array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('EA_PAGE_COUNT'),
			'TYPE' => 'STRING',
			'DEFAULT' => '20',
		)
	)
);