<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Loader;

class EntityComplex extends CBitrixComponent
{
	public $arrDefaultUrlTemplates404 = array(
		'list' => '',
		'add' => 'add/',
		'edit' => 'edit/#ENTITY_ID#/',
		'detail' => '#ENTITY_ID#/'
	);

	public $arrDefaultVariableAliases404 = array();

	public $arrDefaultVariableAliases = array();

	public $arrComponentVariables = array(
		'ENTITY_ID',
		'add',
		'edit'
	);

	public $strComponentPage = 'list';

	public function __construct($objComponent = null)
	{
		parent::__construct($objComponent);

		Loc::loadMessages(__FILE__);
	}

	public function onPrepareComponentParams($arrParams)
	{
		if (!empty($arrParams['PAGE_COUNT']))
		{
			$arrParams['PAGE_COUNT'] = intVal($arrParams['PAGE_COUNT']);
		}

		if (empty($arrParams['PAGE_COUNT'])) $arrParams['PAGE_COUNT'] = 20;

		return $arrParams;
	}

	public function executeComponent()
	{

		if (strcasecmp($this->arParams['SEF_MODE'], 'Y') == 0)
		{
			$this->makeSEFMode();
		}
		else
		{
			$this->makeDefaultMode();
		}

		$this->includeComponentTemplate($this->strComponentPage);
	}

	public function makeSEFMode()
	{
		$arrVariables = array();

		$arrUrlTemplates = \CComponentEngine::makeComponentUrlTemplates($this->arrDefaultUrlTemplates404, $this->arParams['SEF_URL_TEMPLATES']);
		$arrVariableAliases = \CComponentEngine::makeComponentVariableAliases($this->arrDefaultVariableAliases404, $this->arParams['VARIABLE_ALIASES']);

		$objEngine = new \CComponentEngine($this);

		$this->strComponentPage = $objEngine->guessComponentPath(
			$this->arParams['SEF_FOLDER'],
			$arrUrlTemplates,
			$arrVariables
		);

		if (empty($this->strComponentPage))
		{
			$this->strComponentPage = 'list';
		}

		if (strcasecmp($this->strComponentPage, 'detail') == 0 || strcasecmp($this->strComponentPage, 'edit') == 0)
		{
			$arrVariables['ENTITY_ID'] = (!empty($arrVariables['ENTITY_ID']) ? intVal($arrVariables['ENTITY_ID']) : 0);

			if (empty($arrVariables['ENTITY_ID']))
			{
				$this->strComponentPage = 'list';
			}
		}


		\CComponentEngine::initComponentVariables($this->strComponentPage, $this->arrComponentVariables, $arrVariableAliases, $arrVariables);

		$this->arResult = array(
			'FOLDER' => $this->arParams['SEF_FOLDER'],
			'URL_TEMPLATES' => $arrUrlTemplates,
			'VARIABLES' => $arrVariables,
			'ALIASES' => $arrVariableAliases,
		);
	}

	public function makeDefaultMode()
	{
		$arrVariableAliases = \CComponentEngine::makeComponentVariableAliases($this->arrDefaultVariableAliases, $this->arParams['VARIABLE_ALIASES']);

		\CComponentEngine::initComponentVariables(false, $this->arrComponentVariables, $arrVariableAliases, $arrVariables);

		if (isset($arrVariables['ENTITY_ID']) && intval($arrVariables['ENTITY_ID']) > 0)
		{
			if (isset($arrVariables['edit']) && strcasecmp($arrVariables['edit'], 'y') == 0)
				$this->strComponentPage = 'edit';
			else
				$this->strComponentPage = 'detail';
		}
		elseif (isset($arrVariables['add']) && strcasecmp($arrVariables['add'], 'y') == 0)
		{
			$this->strComponentPage = 'add';
		}
		else
			$this->strComponentPage = 'list';

		$this->arResult = array(
			'FOLDER' => '',
			'URL_TEMPLATES' => array(
				'list' => htmlspecialcharsbx($GLOBALS['APPLICATION']->GetCurPage()),
				'add' => htmlspecialcharsbx($GLOBALS['APPLICATION']->GetCurPage()) . '?add=y',
				'edit' => htmlspecialcharsbx($GLOBALS['APPLICATION']->GetCurPage() . '?edit=y&' . $arrVariableAliases['ENTITY_ID'] . '=#ENTITY_ID#'),
				'detail' => htmlspecialcharsbx($GLOBALS['APPLICATION']->GetCurPage() . '?' . $arrVariableAliases['ENTITY_ID'] . '=#ENTITY_ID#'),
			),
			'VARIABLES' => $arrVariables,
			'ALIASES' => $arrVariableAliases
		);
	}

}