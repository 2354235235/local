<?
$MESS['EA_ENTITY_ID'] = 'Идентификатор записи';
$MESS['EA_SEF_MODE_LIST'] = 'Страница общего списка';
$MESS['EA_SEF_MODE_ADD'] = 'Страница добавления сущности';
$MESS['EA_SEF_MODE_EDIT'] = 'Страница редактирования сущности';
$MESS['EA_SEF_MODE_DETAIL'] = 'Страница сущности';
$MESS['EA_PAGE_COUNT'] = 'Количество сущностей на странице списка';