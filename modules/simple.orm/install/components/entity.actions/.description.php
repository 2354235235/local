<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
	'NAME' => Loc::getMessage('EA_COMP_NAME'),
	'DESCRIPTION' => Loc::getMessage('EA_COMP_DESCR'),
	'COMPLEX' => 'Y',
	'PATH' => array(
		'ID' => 'orm',
		'CHILD' => array(
			'ID' => 'entity',
			'NAME' => Loc::getMessage('EA_COMP_SECT'),
			'SORT' => 30,
		)
	)
);