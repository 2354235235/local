<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$APPLICATION->IncludeComponent(
	"orm:entity.add.edit",
	"",
	Array(
		"ENTITY_ID" => $arResult['VARIABLES']['ENTITY_ID']
	)
);

if (!empty($arResult['FOLDER']))
{
	?>
    <a href="<?= $arResult['FOLDER'] ?>"><?= Loc::getMessage('EAE_BACK_TO_LIST') ?></a>
	<?
} ?>