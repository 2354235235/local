<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->IncludeComponent(
	'orm:entity.list',
	'',
	Array(
		'ADD_PAGE' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['add'],
		'DETAIL_PAGE' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['detail'],
		'EDIT_PAGE' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['edit'],
		'PAGE_SIZE' => $arParams['PAGE_COUNT']
	),
	false,
	array('HIDE_ICONS' => 'Y')
);