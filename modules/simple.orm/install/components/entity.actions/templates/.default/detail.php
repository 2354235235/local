<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$APPLICATION->IncludeComponent(
	'orm:entity.detail',
	'',
	Array(
		'ENTITY_ID' => $arResult['VARIABLES']['ENTITY_ID'],
		'EDIT_PAGE' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['edit'],
		'ADD_PAGE' =>  $arResult['FOLDER'].$arResult['URL_TEMPLATES']['add']
	)
);

if (!empty($arResult['FOLDER']))
{
	?>
    <a href="<?= $arResult['FOLDER'] ?>"><?= Loc::getMessage('EAE_BACK_TO_LIST') ?></a>
	<?
} ?>