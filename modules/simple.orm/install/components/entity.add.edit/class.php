<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Application,
	Bitrix\Main\Web\Uri,
	Bitrix\Main\Loader,
	Bitrix\Main\Config\Option,
	Simple\Orm\Test,
	Simple\Orm\TestTable;

class EntityAddEdit extends CBitrixComponent
{
	public $arResult = array();
	public $arParams = array();
	public $objRequest;
	public $strOption;
	protected $arrFieldReq = array();
	protected $arrPost = array();

	public function __construct($objComponent = null)
	{
		parent::__construct($objComponent);

		Loc::loadMessages(__FILE__);
	}

	public function onPrepareComponentParams($arrParams)
	{
		if (!empty($arrParams['ENTITY_ID']))
		{
			$arrParams['ENTITY_ID'] = intVal($arrParams['ENTITY_ID']);
		}

		return $arrParams;
	}

	public function executeComponent()
	{
		if ($this->loadModules())
		{
			$this->setDefault();

			$this->getVariables();

			$this->getStructure();

			$this->checkVariables();

			$this->includeComponentTemplate();
		}
	}

	protected function getStructure()
	{
		$arrStructure = TestTable::getMap();

		$this->arResult['STRUCTURE'] = array();

		if (!empty($arrStructure))
		{
			if (!empty($this->arParams['ENTITY_ID']))
			{
				$this->getEntity();
			}

			foreach ($arrStructure as $strKey => $arrItem)
			{
				$arrOptions = array();

				if (is_object($arrItem) || !empty($arrItem['primary'])) continue;

				if (strcasecmp($strKey, 'user_id') == 0)
				{
					$arrOptions = self::getUsers($this->arrPost[$strKey] ?? $this->arResult['ENTITY'][$strKey]);
				}

				$strReq = 'N';

				if (!empty($arrItem['required']))
				{
					$this->arrFieldReq[] = $strKey;

					$strReq = 'Y';
				}

				$this->arResult['STRUCTURE'][] = array(
					'TITLE' => $arrItem['title'],
					'TYPE' => $arrItem['data_type'],
					'NAME' => $strKey,
					'REQUIRED' => $strReq,
					'OPTIONS' => $arrOptions,
					'DEFAULT_VALUE' => (!empty($this->arResult['ENTITY'][$strKey]) ? $this->arResult['ENTITY'][$strKey] : ''),
					'VALUE' => (!empty($this->arrPost[$strKey]) ? $this->arrPost[$strKey] : '')
				);
			}

			$this->getUserFields();
		}
	}

	public function getUserFields()
	{
		$this->strOption = Option::get('simple.orm', 'uf_code');

		$this->arResult['USER_FIELDS'] = array();

		if (!empty($this->strOption))
		{
			if (!empty($this->arParams['ENTITY_ID']))
			{
				$arrUF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields($this->strOption, $this->arParams['ENTITY_ID']);
			}

			$arrAllowedTypes = array('string', 'integer', 'double');

			$objData = \CUserTypeEntity::GetList(array('sort' => 'desc'), array('ENTITY_ID' => $this->strOption, 'LANG' => LANGUAGE_ID));

			while ($arrRes = $objData->Fetch())
			{
				if (in_array($arrRes['USER_TYPE_ID'], $arrAllowedTypes))
				{
					if (strcasecmp($arrRes['MANDATORY'], 'Y') == 0)
					{
						$this->arrFieldReq[] = $arrRes['FIELD_NAME'];
					}

					$this->arResult['USER_FIELDS'][] = array(
						'TITLE' => (!empty($arrRes['EDIT_FORM_LABEL']) ? $arrRes['EDIT_FORM_LABEL'] : $arrRes['FIELD_NAME']),
						'TYPE' => 'string',
						'NAME' => $arrRes['FIELD_NAME'],
						'REQUIRED' => $arrRes['MANDATORY'],
						'OPTIONS' => array(),
						'DEFAULT_VALUE' => (!empty($arrUF[$arrRes['FIELD_NAME']]['VALUE']) ? $arrUF[$arrRes['FIELD_NAME']]['VALUE'] : ''),
						'VALUE' => (!empty($this->arrPost[$arrRes['FIELD_NAME']]) ? $this->arrPost[$arrRes['FIELD_NAME']] : '')
					);
				}
			}
		}
	}

	public function getVariables()
	{
		if (empty($this->objRequest))
		{
			$this->getRequest();
		}

		if ($this->objRequest->isPost() && check_bitrix_sessid())
		{
			$arrPost = $this->objRequest->getPostList()->toArray();;

			if (!empty($arrPost[$this->arResult['FORM']['POST_PREFIX']]))
			{
				foreach ($arrPost[$this->arResult['FORM']['POST_PREFIX']] as $strKey => $strVal)
				{
					$strEmpty = preg_replace('/\s+/', '', $strVal);

					if (empty($strEmpty))
					{
						$arrPost[$this->arResult['FORM']['POST_PREFIX']][$strKey] = $strEmpty;
					}
				}
			}

			$this->arrPost = $arrPost[$this->arResult['FORM']['POST_PREFIX']];
		}
	}

	public function checkVariables()
	{
		if (!isset($this->arResult['ERRORS'])) $this->arResult['ERRORS'] = array();

		if (!empty($this->arrPost))
		{
			foreach ($this->arrPost as $strKey => $strPost)
			{
				$intKey = array_search($strKey, $this->arrFieldReq);

				if ($intKey !== false && !empty($strPost))
				{
					unset($this->arrFieldReq[$intKey]);
				}
			}

			if (!empty($this->arrFieldReq))
			{
				$this->arResult['ERRORS'][] = array('ID' => 'EMPTY_REQUIRED_FIELDS', 'FIELDS' => $this->arrFieldReq);
			}
			else
			{

				if (!empty($this->arResult['ENTITY']['ID']))
				{
					$this->updateEntity();
				}
				else
				{
					$this->addEntity();
				}
			}
		}
	}

	protected function updateEntity()
	{
		$intId = Test::update($this->arResult['ENTITY']['ID'], $this->arrPost);

		if (!empty($intId))
		{
			$this->updateUserFields($intId);

			if (!empty($this->arResult['FORM']['ACTION']))
			{
				$strRedirect = self::getNewUri($this->arResult['FORM']['ACTION'], array(), array('added' => 'Y'));;

				localRedirect($strRedirect);
			}
		}
		else
		{
			$this->arResult['ERRORS'][] = array('ID' => 'NOT_ADDED');
		}
	}

	protected function addEntity()
	{
		$intId = Test::add($this->arrPost);

		if (!empty($intId))
		{
			$this->updateUserFields($intId);

			if (!empty($this->arResult['FORM']['ACTION']))
			{
				$strRedirect = self::getNewUri($this->arResult['FORM']['ACTION'], array(), array('added' => 'Y'));;

				localRedirect($strRedirect);
			}
		}
		else
		{
			$this->arResult['ERRORS'][] = array('ID' => 'NOT_ADDED');
		}
	}

	protected function updateUserFields($intId)
	{
		if (!empty($this->arResult['USER_FIELDS']) && !empty($intId))
		{
			$arrUpdated = array();

			foreach ($this->arResult['USER_FIELDS'] as $arrField)
			{
				if (!empty($this->arrPost[$arrField['NAME']])) $arrUpdated[$arrField['NAME']] = $this->arrPost[$arrField['NAME']];
			}

			if (!empty($arrUpdated))
			{
				$boolUserFields = $GLOBALS['USER_FIELD_MANAGER']->Update($this->strOption, $intId, $arrUpdated);

				if (empty($boolUserFields))
				{
					return false;
				}
			}
		}

		return true;
	}

	public static function getUsers($intUserId = 0)
	{
		$arrUsers = array();

		$strUserBy = 'id';
		$strUserOrder = 'desc';

		$arrUserFilter = array(
			'ACTIVE' => 'Y'
		);

		$arrUserParams = array(
			'SELECT' => array(),
			'NAV_PARAMS' => array(
				'nTopCount' => 100
			),
			'FIELDS' => array(
				'ID',
				'LOGIN',
				'NAME',
				'LAST_NAME',
				'SECOND_NAME'
			)
		);

		$objUsers = CUser::GetList(
			$strUserBy,
			$strUserOrder,
			$arrUserFilter,
			$arrUserParams
		);

		while ($arrUser = $objUsers->Fetch())
		{
			if (!empty($intUserId) && $intUserId == $arrUser['ID'])
			{
				$arrUsers[] = array(
					'ID' => $arrUser['ID'],
					'NAME' => self::makeUserName($arrUser),
					'SELECTED' => 'Y'
				);
			}
			else
			{
				$arrUsers[] = array(
					'ID' => $arrUser['ID'],
					'NAME' => self::makeUserName($arrUser)
				);
			}
		}

		return $arrUsers;
	}

	public static function makeUserName($arrUser)
	{
		$strUserName = '';

		if (!empty($arrUser['NAME'])) $strUserName = $arrUser['NAME'];

		if (!empty($arrUser['LAST_NAME']))
		{
			if (!empty($strUserName))
			{
				$strUserName .= ' ' . $arrUser['LAST_NAME'];
			}
			else
			{
				$strUserName = $arrUser['LAST_NAME'];
			}
		}

		if (!empty($arrUser['SECOND_NAME']))
		{
			if (!empty($strUserName))
			{
				$strUserName .= ' ' . $arrUser['SECOND_NAME'];
			}
			else
			{
				$strUserName = $arrUser['SECOND_NAME'];
			}
		}

		if (empty($strUserName))
		{
			$strUserName = $arrUser['LOGIN'];
		}

		return $strUserName;
	}

	public function getEntity()
	{
		if (!empty($this->arParams['ENTITY_ID']))
		{
			$this->arResult['ENTITY'] = Test::getById($this->arParams['ENTITY_ID']);
		}
	}

	public function setDefault()
	{
		$this->getRequest();

		$strUri = $this->objRequest->getRequestUri();

		$this->arResult['FORM'] = array();

		$this->arResult['FORM']['ACTION'] = self::getNewUri($strUri, array('added'));

		$this->arResult['FORM']['METHOD'] = 'POST';

		$this->arResult['FORM']['POST_PREFIX'] = 'entity';
	}

	public static function getNewUri($strUri, $arrDelete = array(), $arrAdd = array())
	{
		$strNewUri = $strUri;

		if (!empty($strUri) && (!empty($arrDelete) || !empty($arrAdd)))
		{
			$objNewUri = new Uri($strUri);

			if (!empty($arrDelete))
			{
				$objNewUri->deleteParams($arrDelete);

			}

			if (!empty($arrAdd))
			{
				$objNewUri->addParams($arrAdd);
			}

			$strNewUri = $objNewUri->getUri();
		}

		return $strNewUri;
	}

	public function getRequest()
	{
		if (empty($this->objRequest)) $this->objRequest = Application::getInstance()->getContext()->getRequest();
	}

	protected function loadModules()
	{
		if (empty(Loader::IncludeModule('simple.orm')))
		{
			showError(Loc::getMessage('EAE_MODULE_ORM_ERROR'));
			return false;
		}

		return true;
	}
}