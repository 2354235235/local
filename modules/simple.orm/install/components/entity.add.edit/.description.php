<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
	'NAME' => Loc::getMessage('EAE_COMP_NAME'),
	'DESCRIPTION' => Loc::getMessage('EAE_COMP_DESCR'),
	'PATH' => array(
		'ID' => 'orm',
		'CHILD' => array(
			'ID' => 'entity',
			'NAME' => Loc::getMessage('EAE_COMP_SECT'),
			'SORT' => 40,
		)
	)
);