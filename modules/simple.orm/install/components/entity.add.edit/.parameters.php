<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$arComponentParameters = array(
	'PARAMETERS' => array(
		'ENTITY_ID' => Array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('EAE_ENTITY_ID'),
			'TYPE' => 'STRING',
		)
	)
);