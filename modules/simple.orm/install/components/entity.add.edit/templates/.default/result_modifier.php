<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Application;

$objComp = $this->__component;

$objRequest = $objComp->objRequest;

$strAdd = $objRequest->getQuery('added');

if (!empty($strAdd) && strcasecmp($strAdd, 'Y') == 0)
{
	$arResult['MESSAGE'] = (empty($arResult['ENTITY']) ? Loc::getMessage('EAE_SUCCESS_ADDED'): Loc::getMessage('EAE_SUCCESS_UPDATED'));
}