<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if (!empty($arResult['STRUCTURE']))
{
	?>
    <form method="<?= $arResult['FORM']['METHOD'] ?>" action="<?= $arResult['FORM']['ACTION'] ?>">
		<?= bitrix_sessid_post() ;

		if (!empty($arResult['ERRORS']))
		{
			foreach ($arResult['ERRORS'] as $arrError)
			{
				if (!empty($arrError['ID'])) showError(Loc::getMessage('EAE_' . $arrError['ID']));

				else showError($arrError);
			}
		}

		if(!empty($arResult['MESSAGE']))
        {
            showNote($arResult['MESSAGE']);
        }
        ?>
        <table class="data-table">
			<?
			foreach ($arResult['STRUCTURE'] as $arrItem)
			{
				?>
                <tr>
                    <td>
						<?= $arrItem['TITLE'];
						if (strcasecmp($arrItem['REQUIRED'], 'Y') == 0) echo ' *'; ?>
                    </td>
                    <td>
						<?
						switch ($arrItem['TYPE'])
						{
							case 'string':
							case 'integer':

								if (empty($arrItem['OPTIONS']))
								{
									?>
                                    <input value="<?= (!empty($arrItem['VALUE']) ? $arrItem['VALUE'] : (!empty($arrItem['DEFAULT_VALUE']) ? $arrItem['DEFAULT_VALUE'] : '')) ?>"
                                           name="<?= $arResult['FORM']['POST_PREFIX'] ?>[<?= $arrItem['NAME'] ?>]"
                                           type="text"/>
									<?
								}
								else
								{
									?>
                                    <select name="<?= $arResult['FORM']['POST_PREFIX'] ?>[<?= $arrItem['NAME'] ?>]">
										<?
										foreach ($arrItem['OPTIONS'] as $arrOption)
										{
											?>
                                            <option <? if (!empty($arrOption['SELECTED'])) echo 'selected=""'; ?>
                                                    value="<?= $arrOption['ID'] ?>"><?= $arrOption['NAME'] ?></option>
											<?
										} ?>
                                    </select>
									<?
								}

								break;

							case 'text':
								?>
                                <textarea name="<?= $arResult['FORM']['POST_PREFIX'] ?>[<?= $arrItem['NAME'] ?>]"
                                          cols="30"
                                          rows="10"><?= (!empty($arrItem['VALUE']) ? $arrItem['VALUE'] : (!empty($arrItem['DEFAULT_VALUE']) ? $arrItem['DEFAULT_VALUE'] : '')) ?></textarea>
								<?

								break;
						}
						?>
                    </td>
                </tr>
				<?
			}

			if(!empty($arResult['USER_FIELDS']))
            {
                ?>
                <tr>
                    <td>
                        <b><?=Loc::getMessage('EAE_USER_FIELDS_TITLE')?></b>
                    </td>
                </tr>
                <?

				foreach ($arResult['USER_FIELDS'] as $arrItem)
				{
					?>
                    <tr>
                        <td>
							<?= $arrItem['TITLE'];
							if (strcasecmp($arrItem['REQUIRED'], 'Y') == 0) echo ' *'; ?>
                        </td>
                        <td>
                            <input value="<?= (!empty($arrItem['VALUE']) ? $arrItem['VALUE'] : (!empty($arrItem['DEFAULT_VALUE']) ? $arrItem['DEFAULT_VALUE'] : '')) ?>"
                                   name="<?= $arResult['FORM']['POST_PREFIX'] ?>[<?= $arrItem['NAME'] ?>]"
                                   type="text"/>
                        </td>
                    </tr>
					<?
				}
            }
			?>
        </table>

        <button type="submit"><?=Loc::getMessage('EAE_SAVE')?></button>
    </form>
	<?
}?>