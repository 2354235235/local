<?
$MESS ['EAE_EMPTY_REQUIRED_FIELDS'] = 'Заполните все поля отмеченные звездочкой';
$MESS ['EAE_NOT_ADDED'] = 'Ошибка добавления сущности';
$MESS ['EAE_SUCCESS_ADDED'] = 'Успешно добавлено';
$MESS ['EAE_SUCCESS_UPDATED'] = 'Успешно обновлено';
$MESS ['EAE_USER_FIELDS_TITLE'] = 'Пользовательские поля';
$MESS ['EAE_SAVE'] = 'Сохранить';