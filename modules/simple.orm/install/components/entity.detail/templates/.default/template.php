<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if (!empty($arResult['ENTITY']))
{
	?>
    <div>
		<?
		if (!empty($arResult['ADD_PAGE']))
		{
			?>
            <a href="<?= $arResult['ADD_PAGE'] ?>"><?= Loc::getMessage('ED_ADD_PAGE') ?></a>
			<?
		}

		if (!empty($arResult['EDIT_PAGE']))
		{
			?>
            <a href="<?= $arResult['EDIT_PAGE'] ?>"><?= Loc::getMessage('ED_EDIT_PAGE') ?></a>
			<?
		} ?>
        <h2><?= $arResult['ENTITY']['TITLE'] ?></h2>
		<?
		if (!empty($arResult['ENTITY']['DESCRIPTION']))
		{
			?>
            <div>
				<?= $arResult['ENTITY']['DESCRIPTION'] ?>
            </div>
			<?
		}

		if (!empty($arResult['ENTITY']['FULL_NAME']))
		{
			?>
            <br/>
            <b>
				<?= $arResult['ENTITY']['FULL_NAME'] ?>
            </b>
			<?
		}
        elseif (!empty($arResult['ENTITY']['LOGIN']))
		{
			?>
            <br/>
            <b>
				<?= $arResult['ENTITY']['LOGIN'] ?>
            </b>
			<?
		}

		if (!empty($arResult['ENTITY']['USER_FIELDS']))
		{
			?>
            <h3><?= Loc::getMessage('ED_USER_FIELDS_TITLE'); ?></h3>
			<?
			foreach ($arResult['ENTITY']['USER_FIELDS'] as $arrField)
			{
				?>
                <b><?= (!empty($arrField['EDIT_FORM_LABEL']) ? $arrField['EDIT_FORM_LABEL'] : $arrField['FIELD_NAME']) ?>:</b> <?= (!empty($arrField['VALUE']) ? $arrField['VALUE'] : '-') ?>
                <br/>
                <?
			}
		}
		?>
    </div>
	<?
}
?>

