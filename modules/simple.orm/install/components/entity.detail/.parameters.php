<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$arComponentParameters = array(
	'PARAMETERS' => array(
		'ENTITY_ID' => Array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('ED_ENTITY_ID'),
			'TYPE' => 'STRING',
		),

		'EDIT_PAGE' => Array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('ED_EDIT_PAGE'),
			'TYPE' => 'STRING',
		),

		'ADD_PAGE' => Array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('ED_ADD_PAGE'),
			'TYPE' => 'STRING',
		)
	)
);