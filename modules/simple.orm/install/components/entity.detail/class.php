<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Loader,
	Bitrix\Main\Config\Option,
	Simple\Orm\Test;

class EntityDetail extends CBitrixComponent
{
	public $arResult = array();
	public $arParams = array();

	public function __construct($objComponent = null)
	{
		parent::__construct($objComponent);

		Loc::loadMessages(__FILE__);
	}

	public function onPrepareComponentParams($arrParams)
	{
		if (!empty($arrParams['ENTITY_ID']))
		{
			$arrParams['ENTITY_ID'] = intVal($arrParams['ENTITY_ID']);
		}

		if (!empty($arrParams['EDIT_PAGE']))
		{
			$arrParams['EDIT_PAGE'] = trim($arrParams['EDIT_PAGE']);
		}

		if (!empty($arrParams['DETAIL_PAGE']))
		{
			$arrParams['DETAIL_PAGE'] = trim($arrParams['DETAIL_PAGE']);
		}

		if (!empty($arrParams['ADD_PAGE']))
		{
			$arrParams['ADD_PAGE'] = trim($arrParams['ADD_PAGE']);
		}

		return $arrParams;
	}

	public function executeComponent()
	{
		if ($this->loadModules())
		{
			$this->getEntity();

			if (!empty($this->arResult['ENTITY']))
			{
				$this->makeUrl();

				$this->includeComponentTemplate();

				$GLOBALS['APPLICATION']->setTitle($this->arResult['ENTITY']['TITLE']);
			}
		}
	}

	public function makeUrl()
	{
		if (!empty($this->arParams['ADD_PAGE']))
		{
			$this->arResult['ADD_PAGE'] = \CComponentEngine::MakePathFromTemplate(
				$this->arParams['ADD_PAGE'],
				array()
			);
		}

		if (!empty($this->arParams['EDIT_PAGE']))
		{
			$this->arResult['EDIT_PAGE'] = \CComponentEngine::MakePathFromTemplate(
				$this->arParams['EDIT_PAGE'],
				array(
					'ENTITY_ID' => $this->arResult['ENTITY']['ID']
				)
			);
		}
	}

	public function getEntity()
	{
		if (!empty($this->arParams['ENTITY_ID']))
		{
			$this->arResult['ENTITY'] = Test::getById($this->arParams['ENTITY_ID']);
		}

		if (empty($this->arResult['ENTITY']))
		{
			showError(Loc::getMessage('EL_NO_ENTITY'));
		}
		else
		{
			$this->getUserFields();
		}
	}

	public function getUserFields()
	{
		$strOption = Option::get('simple.orm', 'uf_code');

		if(!empty($strOption))$this->arResult['ENTITY']['USER_FIELDS'] = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields($strOption, $this->arResult['ENTITY']['ID'], LANGUAGE_ID);
	}

	protected function loadModules()
	{
		if (empty(Loader::IncludeModule('simple.orm')))
		{
			showError(Loc::getMessage('EL_MODULE_ORM_ERROR'));
			return false;
		}

		return true;
	}
}