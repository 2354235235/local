<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;


$arComponentParameters = array(
	'PARAMETERS' => array(
		'PAGE_SIZE' => Array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('EL_PAGE_SIZE'),
			'TYPE' => 'STRING',
		),

		'EDIT_PAGE' => Array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('EL_EDIT_PAGE'),
			'TYPE' => 'STRING',
		),

		'DETAIL_PAGE' => Array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('EL_DETAIL_PAGE'),
			'TYPE' => 'STRING',
		),

		'ADD_PAGE' => Array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('EL_ADD_PAGE'),
			'TYPE' => 'STRING',
		)
	)
);