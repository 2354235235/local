<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Grid\Options,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\UI\PageNavigation,
	Bitrix\Main\Loader,
	Simple\Orm\Test,
	Simple\Orm\TestTable;

class EntitiesList extends CBitrixComponent
{
	public $strListId = 'simple_orm';
	public $strNavId = 'request_list';
	public $arrSort = array();

	public function __construct($objComponent = null)
	{
		parent::__construct($objComponent);

		Loc::loadMessages(__FILE__);
	}

	public function onPrepareComponentParams($arrParams)
	{
		if (!empty($arrParams['PAGE_SIZE']))
		{
			$arrParams['PAGE_SIZE'] = intVal($arrParams['PAGE_SIZE']);
		}

		if (!empty($arrParams['ADD_PAGE']))
		{
			$arrParams['ADD_PAGE'] = trim($arrParams['ADD_PAGE']);
		}

		if (!empty($arrParams['EDIT_PAGE']))
		{
			$arrParams['EDIT_PAGE'] = trim($arrParams['EDIT_PAGE']);
		}

		if (!empty($arrParams['DETAIL_PAGE']))
		{
			$arrParams['DETAIL_PAGE'] = trim($arrParams['DETAIL_PAGE']);
		}

		return $arrParams;
	}

	public function executeComponent()
	{
		if ($this->loadModules())
		{
			$this->getSort();

			$this->getNavObject();

			$this->getColumns();

			$this->getEntities();

			$this->includeComponentTemplate();
		}
	}

	public static function makeUrl($strUrl, $intId = 0)
	{
		if (!empty($strUrl))
		{
			$arrChange = array();

			if (!empty($intId))
			{
				$arrChange = array(
					'ENTITY_ID' => $intId
				);
			}

			return \CComponentEngine::MakePathFromTemplate(
				$strUrl,
				$arrChange
			);

		}
	}


	public function getEntities()
	{
		$this->arResult['LIST_ID'] = $this->strListId;

		$objEntities = Test::getList($this->arrSort['sort'], array(), array('*', 'FULL_NAME', 'USER.*', 'LOGIN' => 'USER.LOGIN'), array('offset' => $this->arResult['objNav']->getOffset(), 'limit' => $this->arResult['objNav']->getLimit()));

		$this->arResult['ENTITIES'] = array();

		while ($arrEntity = $objEntities->fetch())
		{

			$this->arResult['ENTITIES'][] = array(
				'data' => array(
					'ID' => $arrEntity['ID'],
					'TITLE' => $arrEntity['TITLE'],
					'DESCRIPTION' => $arrEntity['DESCRIPTION'],
					'FULL_NAME' => !empty($arrEntity['FULL_NAME']) ? $arrEntity['FULL_NAME'] : $arrEntity['LOGIN'],
				),
				'actions' => array(
					array(
						'text' => Loc::getMessage('EA_DETAIL_PAGE_LINK'),
						'default' => true,
						'onclick' => 'document.location.href="' . self::makeUrl($this->arParams['DETAIL_PAGE'], $arrEntity['ID']) . '"'
					),
					array(
						'text' => Loc::getMessage('EA_EDIT_PAGE_LINK'),
						'default' => true,
						'onclick' => 'document.location.href="' . self::makeUrl($this->arParams['EDIT_PAGE'], $arrEntity['ID']) . '"'
					)
				)
			);
		}
	}

	public function getColumns()
	{
		$this->arResult['COLUMNS'] = array(
			array('id' => 'ID', 'name' => 'ID', 'sort' => Loc::getMessage('EL_HEADERS_ID'), 'default' => true),
			array('id' => 'TITLE', 'name' => Loc::getMessage('EL_HEADERS_TITLE'), 'sort' => 'TITLE', 'default' => true),
			array('id' => 'DESCRIPTION', 'name' => Loc::getMessage('EL_HEADERS_DESCRIPTION'), 'sort' => false, 'default' => true),
			array('id' => 'FULL_NAME', 'name' => Loc::getMessage('EL_HEADERS_FULL_NAME'), 'sort' => 'FULL_NAME', 'default' => true)
		);
	}

	public function getSort()
	{
		$objOptions = new Options($this->strListId);

		$this->arrSort = $objOptions->GetSorting(array('sort' => array('ID' => 'DESC'), 'vars' => array('by' => 'by', 'order' => 'order')));
	}

	public function getNavObject()
	{
		if (empty($this->arParams['PAGE_SIZE']))
		{
			$this->arParams['PAGE_SIZE'] = 20;
		}

		$this->arResult['objNav'] = new PageNavigation('request_list');

		$this->arResult['objNav']->setPageSize($this->arParams['PAGE_SIZE']);

		$this->arResult['objNav']->setRecordCount(TestTable::getCount())->initFromUri();
	}

	protected function loadModules()
	{
		if (empty(Loader::IncludeModule('simple.orm')))
		{
			showError(Loc::getMessage('ED_MODULE_ORM_ERROR'));
			return false;
		}

		return true;
	}


}