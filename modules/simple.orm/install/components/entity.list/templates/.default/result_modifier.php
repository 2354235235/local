<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$objComp = $this->__component;

if (!empty($arParams['ADD_PAGE']))
{
	$arResult['ADD_URL'] = $objComp->makeUrl($arParams['ADD_PAGE']);
}