<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if (!empty($arResult['ADD_URL']))
{
	?>
    <a href="<?= $arResult['ADD_URL'] ?>">
		<?= Loc::getMessage('EA_ADD_NEW'); ?>
    </a>
    <br/>
	<?
}

if (!empty($arResult['ENTITIES']))
{
	$APPLICATION->IncludeComponent('bitrix:main.ui.grid', '', array(
		'GRID_ID' => $arResult['LIST_ID'],
		'COLUMNS' => $arResult['COLUMNS'],
		'ROWS' => $arResult['ENTITIES'],
		'SHOW_ROW_CHECKBOXES' => false,
		'NAV_OBJECT' => $arResult['objNav'],
		'AJAX_MODE' => 'Y',
		'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
		'PAGE_SIZES' => false,
		'AJAX_OPTION_JUMP' => 'N',
		'SHOW_CHECK_ALL_CHECKBOXES' => false,
		'SHOW_ROW_ACTIONS_MENU' => true,
		'SHOW_GRID_SETTINGS_MENU' => false,
		'SHOW_NAVIGATION_PANEL' => true,
		'SHOW_PAGINATION' => true,
		'SHOW_SELECTED_COUNTER' => false,
		'SHOW_TOTAL_COUNTER' => false,
		'SHOW_PAGESIZE' => true,
		'SHOW_ACTION_PANEL' => false,
		'ALLOW_COLUMNS_SORT' => true,
		'ALLOW_COLUMNS_RESIZE' => true,
		'ALLOW_HORIZONTAL_SCROLL' => true,
		'ALLOW_SORT' => true,
		'ALLOW_PIN_HEADER' => true,
		'AJAX_OPTION_HISTORY' => 'N'
	),
		false,
		array('HIDE_ICONS' => 'Y',)
	);
}
?>

