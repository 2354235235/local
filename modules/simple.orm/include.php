<?
Bitrix\Main\Loader::registerAutoloadClasses(
	'simple.orm',
	array(
		'Simple\\Orm\\Test' => 'classes/test.php',
		'Simple\\Orm\\DefaultOrm' => 'interfaces/default_orm.php',
	)
);