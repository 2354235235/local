<?

namespace Simple\Orm;

use Bitrix\Main,
	Bitrix\Main\UserTable,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\Relations\Reference,
	Bitrix\Main\ORM\Query\Join;

Loc::loadMessages(__FILE__);

/**
 * Class EntityTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TITLE string(255) mandatory
 * <li> DESCRIPTION string optional
 * <li> USER_ID int mandatory
 * </ul>
 *
 * @package Bitrix\ENTITY
 **/
class TestTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'TEST_ENTITY';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('SO_ENTITY_ID_FIELD'),
			),
			'TITLE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateTitle'),
				'title' => Loc::getMessage('SO_ENTITY_TITLE_FIELD'),
			),
			'DESCRIPTION' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('SO_ENTITY_DESCRIPTION_FIELD'),
			),
			'USER_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('SO_ENTITY_USER_ID_FIELD'),
			),

			(new Reference(
				'USER',
				UserTable::class,
				Join::on('this.USER_ID', 'ref.ID')
			))->configureJoinType('inner'),
			new Main\Entity\ExpressionField(
				'FULL_NAME', 'Trim(CONCAT(IFNULL(%s, ""), " ", IFNULL(%s, ""), " ", IFNULL(%s, "")))', ['USER.LAST_NAME', 'USER.NAME', 'USER.SECOND_NAME']
			),
		);
	}

	/**
	 * Returns validators for TITLE field.
	 *
	 * @return array
	 */
	public static function validateTitle()
	{
		return array(
			new Main\Entity\Validator\Length(null, 256),
		);
	}
}