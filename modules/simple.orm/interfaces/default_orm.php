<?

namespace Simple\Orm;


interface DefaultOrm
{
	public static function getList($arrOrder = array(), $arrFilter = array(), $arrSelect = array(), $arrNav = array('offset' => 0, 'limit' => 0));

	public static function getById($intId = 0);

	public static function add($arrFields = array());

	public static function update($intId = 0, $arrFields = array());

	public static function delete($intId = 0);
}